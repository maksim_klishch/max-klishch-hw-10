import { useSelector } from "react-redux";
import { Snackbar } from "@material-ui/core";

const ResponseError = () => {
    const {
        responseError
    } = useSelector((state) => state.usersManager);

    return (
        <Snackbar 
            anchorOrigin={{vertical: 'top', horizontal:'center'}}
            message="Something going wrong. Try reload page"
            open={!!responseError}
        />
    );
}

export default ResponseError