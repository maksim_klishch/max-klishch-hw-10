import { useSelector } from "react-redux";
import { Table, TableBody, TableCell, TableRow } from "@material-ui/core";

const DisplaySelectedUser = () => {
    const {
        users,
        selectedUserId,
    } = useSelector(state => state.usersManager)

    if(selectedUserId === "") return (<></>)

    const user = users[selectedUserId]

    return (
        <Table>
            <TableBody>
                <TableRow><TableCell>Name:</TableCell><TableCell>{user.name}</TableCell></TableRow>
                <TableRow><TableCell>Email: </TableCell><TableCell>{user.email}</TableCell></TableRow>
                <TableRow><TableCell>Phone: </TableCell><TableCell>{user.phone}</TableCell></TableRow>
                <TableRow><TableCell>Website: </TableCell><TableCell>{user.website}</TableCell></TableRow>
            </TableBody>
        </Table>
    )
}

export default DisplaySelectedUser;