import { useSelector, useDispatch } from "react-redux";
import {
    FormControl,
    InputLabel,
    Select,
    MenuItem
} from "@material-ui/core";
import { setSelectedUser } from "../redux/actions";

const SelectUsers = () => {
    const {
        users,
        selectedUserId,
        isLoading
    } = useSelector((state) => state.usersManager);

    const dispatch = useDispatch();

    const onChange = (e) => {
        const { value } = e.target;
        dispatch(setSelectedUser(value));
    }

    return (
        <FormControl style={{ width: "100%", margin: "48px 0" }} disabled={isLoading}>
            <InputLabel id="demo-simple-select-label">Select Users</InputLabel>
            <Select
                value={selectedUserId}
                onChange={onChange}
                labelId="demo-simple-select-label"
                id="demo-simple-select"
            >
                {!!users.length &&
                    users.map(({ id, name }) => <MenuItem key={id} value={id}>{name}</MenuItem>)
                }
            </Select>
        </FormControl>
    );
};

export default SelectUsers;
