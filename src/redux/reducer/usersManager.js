import {
    USERS_MANAGER_SET_USERS,
    USERS_MANAGER_SET_SELECTED_USER,
    USER_MANAGER_SET_LOADING,
    USER_MANAGER_SET_ERROR
} from "../actions";

const initialState = {
    users: [],
    selectedUserId: "",
    isLoading: false,
    responseError: null
};

const usersManager = (state = initialState, { type, payload }) => {
    switch (type) {
        case USERS_MANAGER_SET_USERS:
            return {
                ...state,
                users: payload
            };

        case USERS_MANAGER_SET_SELECTED_USER:
            return {
                ...state,
                selectedUserId: payload
            }

        case USER_MANAGER_SET_LOADING:
            return {
                ...state,
                isLoading: payload
            }

        case USER_MANAGER_SET_ERROR:
            return {
                ...state,
                responseError: payload
            }

        default:
            return state;
    }
}

export default usersManager;
