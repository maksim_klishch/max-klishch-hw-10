import { API_USERS_URI } from "../../constants.json";

export const USERS_MANAGER_SET_USERS = "usersManager/SET_USERS";
export const USERS_MANAGER_SET_SELECTED_USER = "usersManager/SET_SELECTED_USER";
export const USER_MANAGER_SET_LOADING = "userManager/SET_LOADING";
export const USER_MANAGER_SET_ERROR = "userManager/SET_ERROR";

const REQUEST_TIME = 3000;

export const setUsers = (payload) => ({
    type: USERS_MANAGER_SET_USERS,
    payload
});

export const setSelectedUser = (payload) => ({
    type: USERS_MANAGER_SET_SELECTED_USER,
    payload
});

export const setLoading = (payload) => ({
    type: USER_MANAGER_SET_LOADING,
    payload
});

export const setError = (payload) => ({
    type: USER_MANAGER_SET_ERROR,
    payload
});

export const fetchUsers = () => (dispatch) => {
    dispatch(setLoading(true))
    fetchRequest(API_USERS_URI)
        .then(response => response.json())
        .then(users => dispatch(setUsers(users)))
        .catch(error => dispatch(setError(error)))
        .finally(() => {
            dispatch(setLoading(false))
        })
}

/**
 * Request
 * @param {string} url
 * @return {Promise|Error}
 */
function fetchRequest(url) {
    const rnd = Math.round(Math.random());

    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (!rnd) {
                const error = new Error("An error occurred while executing the request.");

                return reject(error);
            }

            fetch(url)
                .then((response) => resolve(response));
        }, REQUEST_TIME);
    });
}
