import { createStore, applyMiddleware, compose } from "redux";
import reduxThunk from "redux-thunk";
import reducer from "./reducer";

const store = createStore(
    reducer,
    compose(
        applyMiddleware(reduxThunk)
    )
);

export default store;
