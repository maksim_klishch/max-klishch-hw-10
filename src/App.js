import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { fetchUsers } from "./redux/actions";
import { Container } from "@material-ui/core";
import SelectUsers from "./components/SelectUsers";
import Spinner from './components/Spinner'
import './App.css';
import ResponseError from "./components/ResponseError";
import DisplaySelectedUser from "./components/DisplaySelectedUser";

function App() {
  const {
    isLoading
  } = useSelector((state) => state.usersManager);

  const dispatch = useDispatch();

  useEffect(() => {
    fetchUsers()(dispatch)
  }, [dispatch])

  return (
    <Container>
      {isLoading && <Spinner />}
      <ResponseError />

      <Container>
        <SelectUsers />
        <DisplaySelectedUser />
      </Container>
    </Container>
  );
}

export default App;
